#!/usr/bin/env node

const http = require('http');
const fs = require('fs');
const puppeteer = require("/usr/src/app/node_modules/puppeteer");
const path = require('path');

async function takeScreenshot(url) {
    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'], }); // Docker all the way!
    const page = await browser.newPage();
    await page.goto(url);
    url_path = url.replace("/", "_");
    await page.screenshot({ path: "screens/" + url_path });
    await browser.close();
}

/** handle GET request */
async function coolHandler(req, res, reqUrl) {
    console.log("reqUrl", reqUrl);
    url = reqUrl.searchParams.get("url");
    url = (url || "").trim().toLowerCase();
    if (url.startsWith("http")) {
        filepath = await takeScreenshot(url);
        res.writeHead(200);
        res.write('Work done: ', filepath);
        res.end();
        return;
    } else {
        res.writeHead(500);
        res.write('Me no Worky :<');
        res.end();
        return;
    }
}

/** handle GET request */
async function displayHandler(req, res, reqUrl) {

    console.log("reqUrl", reqUrl);
    file_path = (reqUrl.searchParams.get("file") || "").trim();
    if (file_path.length == 0 || file_path.toLowerCase().includes("flag")) {
        res.writeHead(302, { 'Location': '/?file=index.html' });
        res.end();
        return;
    }
    try {
        const data = fs.readFileSync(file_path, 'utf8');
        res.writeHead(200);
        res.write(data);
        res.end();
        return;
    } catch (err) {
        console.log(err);
        res.writeHead(404);
        res.write("File doesn't exist or can't be read :(");
        res.end();
        return;
    }
}

let base_url = "http://0.0.0.0/";

http.createServer((req, res) => {
    // create an object for all redirection options
    const router = {
        'GET/coolish-unguessable-feature': coolHandler,
        'default': displayHandler
    };
    // parse the url by using WHATWG URL API
    let reqUrl = new URL(req.url, base_url);
    // find the related function by searching "method + pathname" and run it
    let redirectedFunc = router[req.method + reqUrl.pathname] || router['default'];
    redirectedFunc(req, res, reqUrl);
}).listen(80, () => {
    console.log('Server is running at ' + base_url);
});
